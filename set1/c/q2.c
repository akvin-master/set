#include<stdio.h>
int perfect(int num){
    for(int i=1;i*i<=num;i++){
        if(i*i==num){
            return 1;
        }
    }
    return 0;
}
int multi(int num){
    if(num%4==0 && num%6==0){
        return 1;
    }
    return 0;
}
int main(){
    int n;
    scanf("%d",&n);
    int arr[n];
    for(int i=0;i<n;i++){
        scanf("%d",&arr[i]);
    }
    int temp[n];
    for(int i=0;i<n;i++){
        int x=arr[i];
        int sum=0;
        if(perfect(x)){
            sum=sum+5;
        }
        else if(multi(x)){
            sum+=4;
        }
        else if(x%2==0){
            sum+=3;
        }
        temp[i]=sum;
    }
    for(int i=0;i<n;i++){
        for(int j=i+1;j<n;j++){
            if(temp[i]>temp[j]){
                int xx=temp[i];
                temp[i]=temp[j];
                temp[j]=xx;
                int xy=arr[i];
                arr[i]=arr[j];
                arr[j]=xy;
            }
        }
    }
    for(int i=0;i<n;i++){
        printf("%d ",arr[i]);
    }
}